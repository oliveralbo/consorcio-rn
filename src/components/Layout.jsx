import React, { Component } from "react";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Text,
} from "native-base";
import { Col, Row, Grid } from 'react-native-easy-grid';

export default function Layout(props) {

    return (
        <Container>



            <Header>
                <Body>
                    <Title>Header</Title>
                </Body>
                <Right />
            </Header>

            <Grid>
                <Content>
                    {props.children}
                </Content>
            </Grid>

            <Footer>
                <FooterTab>
                    <Button full>
                        <Text>Footer</Text>
                    </Button>
                </FooterTab>
            </Footer>


        </Container>
    );

}
