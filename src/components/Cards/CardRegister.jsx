import React, { Component, useState } from "react";
import { StyleSheet } from "react-native";
import {
    Container,
    Header,
    CardItem,
    Text,
    Body,
    Content,
    Input,
    Label,
    Card,
    Button,
    Item,
} from "native-base";

export default function CardRegister(props) {
    const [data, setData] = useState({
        name: "",
        lastName: "",
        celPhone: 0,
        dni: 0,
    });

    const { action, register } = props;

    const validate = () => {
        register(data);
    };

    const handleChange = (val, estado) => {


        setData({
            ...data,
            [estado]:
                estado === "celPhone" || estado === "dni" ? Number(val.nativeEvent.text, 10) : val,
        });
    };
    // console.log(data)
    return (
        <Container>
            <Content padder>
                <Card>
                    <CardItem header bordered>
                        <Text>Registrate acá</Text>
                    </CardItem>
                    <CardItem bordered>
                        <Body>
                            <Item style={styles.item} rounded>
                                <Input
                                    value={data.name}
                                    onChangeText={(val) => handleChange(val, "name")}
                                    placeholder='Nombre'
                                />
                            </Item>
                            <Item style={styles.item} rounded>
                                <Input
                                    value={data.lastName}
                                    onChangeText={(val) => handleChange(val, "lastName")}
                                    placeholder='Apellido'
                                />
                            </Item>
                            <Item style={styles.item} rounded>
                                <Input
                                    keyboardType='number-pad'
                                    value={data.celPhone}
                                    onChange={(val) => handleChange(val, "celPhone")}
                                    placeholder='Telefono'
                                />
                            </Item>
                            <Item style={styles.item} rounded>
                                <Input
                                    keyboardType='number-pad'
                                    value={data.dni}
                                    onChange={(val) => handleChange(val, "dni")}
                                    placeholder='Documento unico'
                                />
                            </Item>
                            <Button primary full onPress={validate}>
                                <Text> Registrate </Text>
                            </Button>
                        </Body>
                    </CardItem>
                    <CardItem footer bordered>
                        <Text onPress={action}>Volver...</Text>
                    </CardItem>
                </Card>
            </Content>
        </Container>
    );
}

const styles = StyleSheet.create({
    item: {
        marginBottom: 9,
    },
});
