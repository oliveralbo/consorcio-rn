import React, { Component } from "react";
import { StyleSheet } from "react-native";
import {
    Container,
    Header,
    CardItem,
    Text,
    Body,
    Content,
    Input,
    Label,
    Card,
    Button,
    Form, Item
} from "native-base";


export default function CardLogin(props) {

    const { action, logger } = props;

    const register = () => {
        onChangeCard()
    }

    return (
        <Container>
            <Content padder>
                <Card>
                    <CardItem header bordered>
                        <Text>Dr. Springolo 773</Text>
                    </CardItem>
                    <CardItem bordered>
                        <Body>
                            <Item style={styles.item} floatingLabel>
                                <Label>Nombre</Label>
                                <Input />
                            </Item>
                            <Item style={styles.item} floatingLabel>
                                <Label>Documento único</Label>
                                <Input />
                            </Item>
                            <Button primary full onPress={logger}>
                                <Text> Ingresar </Text>
                            </Button>
                        </Body>
                    </CardItem>
                    <CardItem footer bordered>
                        <Text onPress={action}>No tenés cuenta, Registrate acá !</Text>
                    </CardItem>
                </Card>
            </Content>
        </Container>
    );

}



const styles = StyleSheet.create({
    item: {
        marginBottom: 9,
    }
});
