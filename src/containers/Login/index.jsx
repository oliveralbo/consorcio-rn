import React, { Component, useState } from "react";
import { StyleSheet, View } from "react-native";
import CardLogin from "../../components/Cards/CardLogin";
import { Col, Row, Grid } from "react-native-easy-grid";
import CardRegister from "../../components/Cards/CardRegister";
import Layout from "../../components/Layout";
import users from '../../utils/api/users'

export default function Login({ navigation }) {
    const [typeLogin, setTypeLogin] = useState(true);

    const onChangeCard = () => {
        setTypeLogin(!typeLogin)
    }

    const logger = () => {
        navigation.navigate("Home")
    }

    const register = async (data) => {

        // console.log(data)
        users.addUsers(data);
        // let pepe = await users.getWords(); gtewords no va es users
        console.log(pepe)
    }

    return (
        <Layout>
            {typeLogin ? (
                <View style={styles.col1}>
                    <CardLogin action={onChangeCard} logger={logger} />
                </View>
            ) : (
                    <View style={styles.col2}>
                        <CardRegister action={onChangeCard} register={register} />
                    </View>
                )}
        </Layout>
    );
}

const styles = StyleSheet.create({
    col1: {
        marginTop: 100,
    },
    col2: {
        marginTop: 40,
    },
});
